# liquibook

### CLI help message

    Buy: Create a new Buy order and add it to the book
    Sell: Create a new Sell order and add it to the book
      Arguments for BUY or SELL
         <Quantity>
         <Symbol>
         <Price> or MARKET
         AON            (optional)
         IOC            (optional)
         STOP <Price>   (optional)
         ;              end of order

    Modify: Request Modify an existing order
      Arguments:
         <order#>
         PRICE <new price>
         QUANTITY <new initial quantity>
         ;              end of modify requet

    Cancel: Request cancel an existing order
      Arguments:
         <order#>
         ;              end of cancel request (optional)

    Display: Display status of an existing order
      Arguments:
         +       (enable verbose display)
         <order#> or <symbol> or "all"

    (F)ile  Open or Close a command input file
            Arguments
                    <FileName>  Required if no file is open. Must not appear if file is open.
    QUIT  Exit from this program.
