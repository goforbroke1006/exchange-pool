#include <iostream>

#include "../liquibook/examples/mt_order_entry/Market.h"
#include "../liquibook/examples/mt_order_entry/Util.h"

using namespace std;
using namespace orderentry;

int main(int argc, const char *argv[]) {
    if (argc == 1) {
        cerr << "You should specify command!";
        return -1;
    }
    
    vector<string> words;
    for (int i = 1; i < argc; i++) {
        words.emplace_back(argv[i]);
    }

    Market market(&std::cout);
    if (!market.apply(words)) {
        std::cerr << "Cannot process command";
        for (auto word = words.begin(); word != words.end(); ++word) {
            std::cerr << ' ' << *word;
        }
        std::cerr << std::endl;
        return -1;
    }
    return 0;
}