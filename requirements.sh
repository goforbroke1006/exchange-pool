#!/usr/bin/env bash

if [[ ! -d liquibook/ ]]; then
    git clone -b master https://github.com/objectcomputing/liquibook.git
fi
cat <<EOT > liquibook/CMakeLists.txt
cmake_minimum_required(VERSION 3.8)
set(CMAKE_CXX_STANDARD 11)
set(EXECUTABLE_OUTPUT_PATH "../build/")
include_directories(src)
project(example__mt_order_entry)
add_executable(example__mt_order_entry
        examples/mt_order_entry/mt_order_entry_main.cpp
        examples/mt_order_entry/OrderFwd.h
        examples/mt_order_entry/Order.h examples/mt_order_entry/Order.cpp
        examples/mt_order_entry/Util.h examples/mt_order_entry/Util.cpp
        examples/mt_order_entry/Market.h examples/mt_order_entry/Market.cpp
        )
EOT
cd liquibook/
rm -rf .git
cmake .
make